class lysipmi {
  file { '/opt/configure-ipmi.sh':
    ensure => present,
    source => 'puppet:///modules/lysipmi/configure-ipmi.sh',
    mode   => '0700',
    owner  => 'root',
    group  => 'root',
    notify => Exec['set up IPMI'],
  }

  package { 'ipmitool':
    ensure => present,
  }

  exec { 'set up IPMI':
    command     => '/opt/configure-ipmi.sh',
    path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    refreshonly => true,
    require     => Package['ipmitool'],
  }
}
